<div class="container">
    <div id="controls-carousel" class="relative w-full" data-carousel="static">
        <!-- Carousel wrapper -->
        <div class="relative h-56 rounded-lg md:h-96">
            <!-- Item 1 -->

            <!-- Item 2 -->
            <div class="hidden duration-700 ease-in-out" data-carousel-item="active">
                <!-- sushi items -->
                <div class="cotainer mx-auto max-w-[70rem]">
                    <div class="grid grid-cols-3 gap-9">
                        <!-- card 1 -->
                        <div
                            class="max-w-sm bg-gray-300 border rounded-lg shadow-lg group dark:bg-gray-800 dark:border-gray-700">
                            <a href="#">
                                <img class="rounded-t-lg"
                                    src="https://plus.unsplash.com/premium_photo-1670333291474-cb722ca783a5?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                    alt="" />
                            </a>
                            <div class="p-5">
                                <a href="#">
                                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Don't Buy
                                    </h5>
                                </a>
                                <p class="mb-3 text-gray-700 dark:text-gray-400 font-semibold">2.00 ETH</p>
                                <div class="hidden group-hover:block">
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="asep">
                                        <i class="fa-regular fa-heart pe-3"></i>
                                        <p class="inline-flex">Like</p>
                                    </a>
                                    <a href="#"
                                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800">
                                        <p><i class="fa-solid fa-tag pr-2"></i> Buy Now</p>
                                    </a>
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="sam">
                                        <i class="fa-solid fa-cart-shopping" style="padding-right: 0.97em;"></i>
                                        <p class="text-nowrap">Add to cart</p>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <!-- card 2 -->
                        <div
                            class="max-w-sm bg-gray-300 border rounded-lg shadow-lg group dark:bg-gray-800 dark:border-gray-700">
                            <a href="#">
                                <img class="rounded-t-lg"
                                    src="https://images.unsplash.com/photo-1579871494447-9811cf80d66c?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                    alt="" />
                            </a>
                            <div class="p-5">
                                <a href="#">
                                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        It's Expensive
                                    </h5>
                                </a>
                                <p class="mb-3 text-gray-700 dark:text-gray-400 font-semibold">0.32 ETH</p>
                                <div class="hidden group-hover:block">
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="asep">
                                        <i class="fa-regular fa-heart pe-3"></i>
                                        <p class="inline-flex">Like</p>
                                    </a>
                                    <a href="#"
                                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800">
                                        <p><i class="fa-solid fa-tag pr-2"></i> Buy Now</p>
                                    </a>
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="sam">
                                        <i class="fa-solid fa-cart-shopping" style="padding-right: 0.97em;"></i>
                                        <p class="text-nowrap">Add to cart</p>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <!-- card 3 -->
                        <div
                            class="max-w-sm bg-gray-300 border rounded-lg shadow-lg group dark:bg-gray-800 dark:border-gray-700">
                            <a href="#">
                                <img class="rounded-t-lg"
                                    src="https://images.unsplash.com/photo-1611143669185-af224c5e3252?q=80&w=1932&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                    alt="" />
                            </a>
                            <div class="p-5">
                                <a href="#">
                                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        You Poor
                                    </h5>
                                </a>
                                <p class="mb-3 text-gray-700 dark:text-gray-400 font-semibold">1.09 ETH</p>
                                <div class="hidden group-hover:block">
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="asep">
                                        <i class="fa-regular fa-heart pe-3"></i>
                                        <p class="inline-flex">Like</p>
                                    </a>
                                    <a href="#"
                                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800">
                                        <p><i class="fa-solid fa-tag pr-2"></i> Buy Now</p>
                                    </a>
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="sam">
                                        <i class="fa-solid fa-cart-shopping" style="padding-right: 0.97em;"></i>
                                        <p class="text-nowrap">Add to cart</p>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Item 3 -->
            <div class="hidden duration-700 ease-in-out" data-carousel-item>
                <div class="cotainer mx-auto max-w-[70rem]">
                    <div class="grid grid-cols-3 gap-9">
                        <!-- card 1 -->
                        <div
                            class="max-w-sm bg-gray-300 border rounded-lg shadow-lg group dark:bg-gray-800 dark:border-gray-700">
                            <a href="#">
                                <img class="rounded-t-lg"
                                    src="https://images.unsplash.com/photo-1549206464-82c129240d11?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                    alt="" />
                            </a>
                            <div class="p-5">
                                <a href="#">
                                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Worth It
                                    </h5>
                                </a>
                                <p class="mb-3 text-gray-700 dark:text-gray-400 font-semibold">3.00 ETH</p>
                                <div class="hidden group-hover:block">
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="asep">
                                        <i class="fa-regular fa-heart pe-3"></i>
                                        <p class="inline-flex">Like</p>
                                    </a>
                                    <a href="#"
                                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800">
                                        <p><i class="fa-solid fa-tag pr-2"></i> Buy Now</p>
                                    </a>
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="sam">
                                        <i class="fa-solid fa-cart-shopping" style="padding-right: 0.97em;"></i>
                                        <p class="text-nowrap">Add to cart</p>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <!-- card 2 -->
                        <div
                            class="max-w-sm bg-gray-300 border rounded-lg shadow-lg group dark:bg-gray-800 dark:border-gray-700">
                            <a href="#">
                                <img class="rounded-t-lg"
                                    src="https://images.unsplash.com/photo-1578985545062-69928b1d9587?q=80&w=1989&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                    alt="" />
                            </a>
                            <div class="p-5">
                                <a href="#">
                                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Brown
                                    </h5>
                                </a>
                                <p class="mb-3 text-gray-700 dark:text-gray-400 font-semibold">12.34 MATRIX</p>
                                <div class="hidden group-hover:block">
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="asep">
                                        <i class="fa-regular fa-heart pe-3"></i>
                                        <p class="inline-flex">Like</p>
                                    </a>
                                    <a href="#"
                                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800">
                                        <p><i class="fa-solid fa-tag pr-2"></i> Buy Now</p>
                                    </a>
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="sam">
                                        <i class="fa-solid fa-cart-shopping" style="padding-right: 0.97em;"></i>
                                        <p class="text-nowrap">Add to cart</p>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <!-- card 3 -->
                        <div
                            class="max-w-sm bg-gray-300 border rounded-lg shadow-lg group dark:bg-gray-800 dark:border-gray-700">
                            <a href="#">
                                <img class="rounded-t-lg"
                                    src="https://images.unsplash.com/photo-1579373903781-fd5c0c30c4cd?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                    alt="" />
                            </a>
                            <div class="p-5">
                                <a href="#">
                                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Good Vibes
                                    </h5>
                                </a>
                                <p class="mb-3 text-gray-700 dark:text-gray-400 font-semibold">0.12 ETH</p>
                                <div class="hidden group-hover:block">
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="asep">
                                        <i class="fa-regular fa-heart pe-3"></i>
                                        <p class="inline-flex">Like</p>
                                    </a>
                                    <a href="#"
                                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800">
                                        <p><i class="fa-solid fa-tag pr-2"></i> Buy Now</p>
                                    </a>
                                    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
                                        id="sam">
                                        <i class="fa-solid fa-cart-shopping" style="padding-right: 0.97em;"></i>
                                        <p class="text-nowrap">Add to cart</p>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Slider controls -->
        <button type="button"
            class="absolute top-0 start-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
            data-carousel-prev>
            <span
                class="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                <svg class="w-4 h-4 text-white dark:text-gray-800 rtl:rotate-180" aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M5 1 1 5l4 4" />
                </svg>
                <span class="sr-only">Previous</span>
            </span>
        </button>
        <button type="button"
            class="absolute top-0 end-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
            data-carousel-next>
            <span
                class="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                <svg class="w-4 h-4 text-white dark:text-gray-800 rtl:rotate-180" aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="m1 9 4-4-4-4" />
                </svg>
                <span class="sr-only">Next</span>
            </span>
        </button>
    </div>
</div>

    <!-- testing -->
    <!-- like button -->
    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
        id="asep">
        <i class="fa-regular fa-heart pe-3"></i>
        <p class="inline-flex">Like</p>
    </a>
    <!-- add to cart button -->
    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
        id="sam">
        <i class="fa-solid fa-cart-shopping pe-3"></i>
        <p class="text-nowrap">Add to cart</p>
    </a>
    <!-- upvote -->
    <a class="inline-flex items-center px-3 py-2 text-sm cursor-pointer group font-medium text-center overflow-hidden text-white bg-teal-400 rounded-lg hover:bg-teal-500 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-teal-600 dark:hover:bg-teal-400 dark:focus:ring-blue-800"
        id="sama">
        <i class="fa-regular fa-circle-up pe-3 text-lg"></i>
        <p class="text-nowrap">Upvote</p>
    </a>